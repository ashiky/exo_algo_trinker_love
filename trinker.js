const { push } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        let nMale = []
        p.forEach(person => {
            if (person.gender == "Male"){
                nMale.push(person)
            }
        });
        return nMale;
    },

    allFemale: function(p){
        let nFemale = []
        p.forEach(person => {
            if (person.gender == "Female"){
                nFemale.push(person)
            }
        });
        return nFemale;
    },

    nbOfMale: function(p){
        return this.allMale(p).length
    },

    nbOfFemale: function(p){
        return this.allFemale(p).length
    },

    nbOfMaleInterest: function(p){
        let nMaleIn = []
        p.forEach(person => {
            if (person.looking_for == "M"){
               nMaleIn.push(person)
            }
        });
        return nMaleIn.length; 
    },

    nbOfFemaleInterest: function(p){
        let nFemaleIn = []
        p.forEach(person => {
            if (person.looking_for == "F"){
                nFemaleIn.push(person)
            }
        });
        return nFemaleIn.length; 
    },
    moreThanIncome: function(p,income){
        let moreIncome = []
        p.forEach(person => {
           let incomeInt = person.income.replace('$', '');
            if (Number(incomeInt) > income){
                moreIncome.push(person)
            }
        });
        return moreIncome 
    },
    moreThanIncomeL: function(p,income) {
        return this.moreThanIncome(p,income).length
    },
    prefMovie: function(p,prefMovie){
        let nLikeMovie = []
        p.forEach(person => {
            if (person.pref_movie.includes(prefMovie)){
                nLikeMovie.push(person)
            }
        });
        return nLikeMovie.length; 
    },
    nFemaleLikeSF: function(p){
        let nFemaleSF = []
        this.allFemale(p).forEach(person => {
            if (person.pref_movie.includes("Sci-Fi")){
                nFemaleSF.push(person)
            }
        });
        return nFemaleSF.length; 
    },
    moreThan1482LikeDoc: function(p){
        let moreThan1482 = []
        this.moreThanIncome(p,1482).forEach(person => {
            if (person.pref_movie.includes("Documentary")){
                moreThan1482.push(person)
            }
        });
        return moreThan1482.length; 
    },
    NameIdIncome4000: function(p){
        let NameIdIncome4000 = []
        this.moreThanIncome(p,4000).forEach(person => {
            NameIdIncome4000.push([person.id,person.first_name,person.last_name])
        });
        return NameIdIncome4000.length; 
    },

    riche: function(p){
        let richest = []
        let riche = 0
        this.allMale(p).forEach(person => {
            if(Number(person.income.replace("$","")) > riche) {
                richest = []
                riche = Number(person.income.replace("$",""))
                richest.push(person.id,person.first_name,person.last_name)
            }
        });
        return richest; 
    },
    average: function(p){
           let some = 0
            p.forEach(person =>{
                some += Number(person.income.replace("$",""))
            });
           return some / p.length;
    },
    match: function(p){
        return "not implemented".red;
    }
}